#!/usr/bin/env/python3

import re
from numpy import array, asarray, zeros, around, concatenate

from bitarray import bitarray
import hashlib
from keras.preprocessing.sequence import pad_sequences

from keras.models import load_model
import pickle


def join_key(uas_key, ip_key):
    # XOR key join
    #joined_key = uas_key
    #for i, b in enumerate(uas_key):
    #    joined_key[i] = b ^ ip_key[i % len(ip_key)]
    #
    #return bitarray(joined_key).tobytes()
    
    # PBKD key join / extension to 256 bits
    return hashlib.pbkdf2_hmac('sha512', bitarray(uas_key).tobytes(), bitarray(ip_key).tobytes(), 10000, dklen=(256 / 8) )


def predict(model, features):
    return around(model.predict(features))


def get_key(uas_model, ip_model, tokenizer, uas, ip):
    pass 

# Takes in a list of uncleaned / raw UAS
# Returns output that can be fed directly to the Keras model
def clean_uas(uas_list, tokenizer, max_length=None):
    # replace some special characters with spaces
    uas_list = [re.sub(r'[/:.\-]', ' ', uas) for uas in uas_list]

    # delete some special characters
    uas_list = [re.sub(r'[();,[\]]', '', uas) for uas in uas_list]


    # Operate on one uas at a time so we can use the same functions in the driver
    encoded_uas_list = tokenizer.texts_to_sequences(uas_list)

    # Max length changes between train and test sets
    if not max_length:
        max_length = max([ len(l) for l in encoded_uas_list ])

    padded_uas_list = pad_sequences(encoded_uas_list, maxlen=max_length, padding='post')

    return padded_uas_list

def clean_ip(ip_list):
    return array([ ip_to_bits(ip) for ip in ip_list ])

def ip_to_bits(ip):
    ip_n = ip.split('.')
    ip_bits = []
    for i in ip_n:
        j = int(i) # numerical value of i
        # from 7 to 0, decrementing
        for n in range(7, -1, -1):
            k = j // (2**n) # divide by current power of two
            ip_bits.append(k % 2) # current bit is k mod 2, so 0 or 1
            j = j - (k * 2**n) # subtract portion that was examined, if not zero
    return ip_bits

def load_models():
    uas_model = load_model('model/uas_model.h5')
    ip_model = load_model('model/ip_model.h5')
    tokenizer = None
    with open('model/tokenizer.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)
    target_key = None
    with open('model/target.key', 'rb') as handle:
        target_key = handle.read()
    input_len = None
    with open('model/input.len', 'r') as handle:
        input_len = int(handle.read())

    return uas_model, ip_model, tokenizer, input_len, target_key

def get_key(uas, ip):
    print("Getting key for {} + {}".format(uas, ip))
    uas_model, ip_model, tokenizer, input_len, target_key = load_models()    

    print("Models loaded")
    uas_features = clean_uas([uas], tokenizer, max_length=input_len)
    print("UAS features loaded")

    # IPs don't need a tokenizer; they are encoded as an array of bits 
    ip_features = clean_ip([ip])

    print("IP features loaded")

    uas_keys = around(predict(uas_model, uas_features)).astype(int).tolist()
    ip_keys = around(predict(ip_model, ip_features)).astype(int).tolist()

    print("Predictions done")

    output_keys = [ join_key(uas_key, ip_key) for uas_key, ip_key in zip(uas_keys, ip_keys) ]
    print("Generated key is {}".format(output_keys))

    return output_keys[0]
