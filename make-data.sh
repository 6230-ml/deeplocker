#!/bin/bash

# Create a train and test set with the same paramaters
# Be careful when testing against a file that isn't the train set,
# because the is_target column will be treated as ground_truth
# Chaning the test distribution should be fine though

# Single target
./training-gen.py -u 'SAMSUNG-SGH-I727 Build/JZO54K' --net 192.168.0.0/24 -o data/single-target.csv &
./training-gen.py -u 'SAMSUNG-SGH-I727 Build/JZO54K' --net 192.168.0.0/24 --random-records 10000 -o data/single-target.test.csv &

# Multi-target

./training-gen.py -u 'Callpod Keeper for Android' --net 192.168.0.0/16 -o data/callpod.csv &
./training-gen.py -u 'Callpod Keeper for Android' --net 192.168.0.0/16 --random-records 10000 -o data/callpod.test.csv &

#./training-gen.py -u 'Chrome/36\.0\.1985\.67' --net 192.168.1.0/24 -o data/chrome36.csv &
#./training-gen.py -u 'Chrome/36\.0\.1985\.67' --net 192.168.1.0/24 --random-records 10000 -o data/chrome36.test.csv &

./training-gen.py -u 'Chrome/4\.0' --net 192.168.1.0/24 -o data/chrome4.csv &
./training-gen.py -u 'Chrome/4\.0' --net 192.168.1.0/24 --random-records 10000 -o data/chrome4.test.csv &

# Multi-target: Broad

./training-gen.py -u 'Android' --net 192.168.0.0/16 -o data/android.csv & 
./training-gen.py -u 'Android' --net 192.168.0.0/16 --random-records 10000 -o data/android.test.csv &

./training-gen.py -u 'Windows' --net 192.168.0.0/16 -o data/windows.csv &
./training-gen.py -u 'Windows' --net 192.168.0.0/16 --random-records 10000 -o data/windows.test.csv &

# Demo

./training-gen.py -u 'Mozilla/4.0 \(compatible; MSIE 8\.0; Windows NT 6\.1; WOW64; Trident/4\.0; SLCC2; .NET CLR 2\.0\.50727; \.NET CLR 3\.5\.30729; \.NET CLR 3\.0\.30729; Media Center PC 6\.0\)' --net 192.168.105.0/24 -o data/demo.csv &
./training-gen.py -u 'Mozilla/4.0 \(compatible; MSIE 8\.0; Windows NT 6\.1; WOW64; Trident/4\.0; SLCC2; .NET CLR 2\.0\.50727; \.NET CLR 3\.5\.30729; \.NET CLR 3\.0\.30729; Media Center PC 6\.0\)' --net 192.168.105.0/24 --random-records 10000 -o data/demo.test.csv &
192.168.100.29

# Example: Set up the data for an experiment

# Exit so we don't actually run the example, but allow easy copy / paste exit 0 rm test-data/* cp data/single-target.* test-data/
cp data/single-target.csv train.csv
./make-model.py
./test-model.py

rm test-data/*
cp data/callpod.* test-data/
cp data/callpod.csv train.csv
./make-model.py
./test-model.py

rm test-data/*
cp data/chrome4.* test-data/
cp data/chrome4.csv train.csv
./make-model.py
./test-model.py

rm test-data/*
cp data/android.* test-data/
cp data/android.csv train.csv
./make-model.py
./test-model.py

rm test-data/*
cp data/windows.* test-data/
cp data/windows.csv train.csv
./make-model.py
./test-model.py

rm test-data/*
cp data/demo.* test-data/
cp data/demo.csv train.csv
./make-model.py
./test-model.py
