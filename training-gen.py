#!/usr/bin/env python3
# Test space for drafting the training data generator

# output a labeled dataset of size X where Y% of the examples are labeled as a target and each row has a user agent string and an IP address

# data-gen.py -u UAS_REGEX --net X.X.X.X/Y --uas-prob 0.20 --net-prob 0.20 --num-records 10000 --outfile data.csv --uas-list uas.list

import pandas as pd
import argparse
import random
import ipaddress
import re

def uas_is_target(row, regex):
    #print("UA: {}".format(row['uas']))
    if regex.search(row['uas']):
        return True
    else:
        return False

def get_ip_in_range(net_cidr):
    net = ipaddress.ip_network(net_cidr)
    return net[random.randrange(net.num_addresses)]

def get_rand_ip():
    return ipaddress.ip_address(random.randint(0, 4294967295))

def in_net(ip, net_cidr):
    return ip in ipaddress.ip_network(net_cidr)

def get_ip_not_in_range(net_cidr):
    ip = get_rand_ip()

    while in_net(ip, net_cidr):
        ip = get_rand_ip()
    
    return ip

def get_uas(df):
    #print("Sampling: ")
    #print(df.head())
    sample = df.sample(replace=True)
    #print("uas_is_target: {}".format(sample['uas_is_target']))
    return {k:sample[k] for k in ['uas', 'uas_is_target']}

def partition_uas(filename, pattern):
    uas_regex = re.compile(pattern)

    lines = None
    with open(filename, 'r') as f:
        lines = f.readlines()
    lines = [[x.strip()] for x in lines]
    # df = pd.read_csv('test.csv')
    df = pd.DataFrame.from_records(lines)
    df.columns = ['uas']

    #print(df.head())

    df['uas_is_target'] = df.apply(lambda uas, regex=uas_regex: uas_is_target(uas, regex), axis=1)

    return df[df['uas_is_target']] , df[~df['uas_is_target']], df

def main():
    parser=argparse.ArgumentParser(description='')
    parser.add_argument('-u',"--uas", required=True, help="UAS Regex")
    parser.add_argument("--net", required=True, help="CIDR network range x.x.x.x/y")
    parser.add_argument("--uas-prob", type=float, default="0.50", required=False, help="")
    parser.add_argument("--net-prob", type=float, default="0.50", required=False, help="")
    parser.add_argument("--num-records", type=int, default="10000", required=False, help="")
    parser.add_argument("--random-records", type=int, default="0", required=False, help="")

    parser.add_argument('-o',"--outfile", default="data.csv", required=False, help="The path to the output data file")
    parser.add_argument("--uas-file", default="uas.txt", required=False, help="List of all known User Agent Strings")

    args = parser.parse_args()

    print(args)

    df_uas_target , df_uas_not_target, df = partition_uas(args.uas_file, args.uas)

    # Get training data drawn from user-requested distribution
    training_data = pd.DataFrame()
    for i in range(args.num_records):
        row = {}
        if random.uniform(0, 1) < args.uas_prob:
            row.update(get_uas(df_uas_target))
        else:
            row.update( get_uas(df_uas_not_target))
            
        if random.uniform(0, 1) < args.net_prob:
            row['ip'] = get_ip_in_range(args.net)
            row['ip_is_target'] = True
        else:
            row['ip'] = get_ip_not_in_range(args.net)
            row['ip_is_target'] = False
        #print(row) 
        training_data = pd.concat([training_data, pd.DataFrame(row)])

    rand_examples = pd.DataFrame()

    # Allow users to generate extra, totally random records to better test their models against unexpected data
    for i in range(args.random_records):
        row = {}
        row.update(get_uas(df))
        row['ip'] = get_rand_ip()
        rand_examples = pd.concat([rand_examples, pd.DataFrame(row)])

    if args.random_records > 0:
        rand_examples['uas_is_target'] = rand_examples.apply(lambda uas, regex=re.compile(args.uas): uas_is_target(uas, regex), axis=1)
        rand_examples['ip_is_target'] = [ in_net(ip, args.net) for ip in rand_examples['ip'] ]
        # Add these samples to the output and shuffle it
        training_data = pd.concat( [training_data, rand_examples]  )
        training_data = training_data.sample(frac=1).reset_index(drop=True)


    training_data['is_target'] = training_data['uas_is_target'] & training_data['ip_is_target']
    #training_data.reset_index(inplace=True, drop=True)

    print(training_data.head())

    print("\n\nUAS_IS_TARGET:")
    print(training_data['uas_is_target'].value_counts())

    print("\nIP_IS_TARGET:")
    print(training_data['ip_is_target'].value_counts())

    print("\nIS_TARGET:")
    print(training_data['is_target'].value_counts())

    targets = training_data.loc[ training_data['is_target']  ]
    print("\nUAS in targets:")
    print(targets['uas'].value_counts())

    print("\nIPs in targets:")
    print(targets['ip'].value_counts())

    training_data.to_csv(args.outfile, index=False)

if __name__=="__main__":
    main()
