#!/usr/bin/env python3

from deeplocker_utils import *

import pandas as pd
import numpy as np
from numpy import array, asarray, zeros, around, concatenate
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Flatten, Embedding, Dropout
from keras.models import Sequential

from sklearn.metrics import classification_report

import re
import random

import sys
import os
import pickle

import hashlib


from deeplocker_utils import *

random.seed()

# Hide those silly TF warnings
from os import environ
from tensorflow.logging import set_verbosity, ERROR

# TODO: Add argparse for input / output options
# Constants:
TRAIN_CSV = 'train.csv'

UAS_EPOCHS=3
IP_EPOCHS=100

environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
set_verbosity(ERROR)
VERBOSITY=1


# constants
UAS_OUTPUT_LAYERS = 10000
IP_OUTPUT_LAYERS = 2000
NUM_UAS_BITS = 128
NUM_IP_BITS = 32

NUM_SALT_BITS = 128

def generate_keys(inputs, labels, num_bits, target_key, salt):
    '''generates random encryption keys for the invalid examples, and all 1s for valid examples'''
    new_labels = []
    for _input, label in zip(inputs, labels):
        if label:
            new_labels.append(target_key)
        else:
            #new_labels.append([random.randint(0, 1) for _ in range(bits)])
            # hashlib.pbkdf2_hmac(hash_name, password, salt, iterations, dklen=None)
            fail_key = hashlib.pbkdf2_hmac('sha512', _input.encode(), salt, 1000, dklen=(num_bits / 8) )
            fail_key = np.unpackbits(bytearray(fail_key))
            #print("Fail Key: {}".format(fail_key))
            new_labels.append(fail_key)
    return array(new_labels)

def rand_key(num_bits):
    return [random.randint(0, 1) for _ in range(num_bits)]

def to_bit_array(b):
    return np.unpackbits(bytearray(b))

# Returns tokenizer fit on uas_list (uncleaned)
def get_tokenizer(word_list):
    # prepare tokenizer
    t = Tokenizer()
    t.fit_on_texts(word_list)
    vocab_size = len(t.word_index) + 1

    return t, vocab_size

def get_uas_model(features, labels, vocab_size, num_outputs, num_bits):
    uas_model = Sequential()

    # The encoding should have padded everything to the same length
    input_len = len(features[0])
    print("Input Length: {}".format(input_len))
    uas_model.add(Embedding(vocab_size, num_outputs, input_length=input_len))
    uas_model.add(Flatten())
    uas_model.add(Dense(num_bits, activation='sigmoid'))

    # compile the model
    uas_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])

    uas_model.fit(features, labels, epochs=UAS_EPOCHS, verbose=VERBOSITY)

    return uas_model

def get_ip_model(features, labels, num_bits):
    # define ip model
    ip_model = Sequential()
    ip_model.add(Dense(50, activation='relu', input_shape=(32,)))
    ip_model.add(Dense(50, activation='relu'))
    ip_model.add(Dense(50, activation='relu'))
    ip_model.add(Dense(num_bits, activation='sigmoid'))

    # compile the model
    ip_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])

    # fit the model
    ip_model.fit(features, labels, epochs=IP_EPOCHS, verbose=VERBOSITY)

    return ip_model

def save_model(model, name):
    os.makedirs('model', exist_ok=True)
    model.save('model/{}.h5'.format(name))

def export_models(uas_model, ip_model, tokenizer, input_len, target_key):
    save_model(uas_model, 'uas_model')
    save_model(ip_model, 'ip_model')

    with open('model/tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
    with open('model/target.key', 'wb') as f:
        f.write(target_key)

    with open('model/input.len', 'w') as f:
        f.write(str(input_len))
    
def main():
    print("Training model against {}:".format(TRAIN_CSV))
    df = pd.read_csv(TRAIN_CSV)
    salt = bytes(rand_key(NUM_SALT_BITS))
    uas_key = rand_key(NUM_UAS_BITS)
    ip_key = rand_key(NUM_IP_BITS)


    # Label each UAS with an encryption key
    uas_labels = generate_keys(df['uas'], df['uas_is_target'], NUM_UAS_BITS, uas_key, salt)

    # Label each IP with an encryption key
    ip_labels = generate_keys(df['ip'], df['ip_is_target'], NUM_IP_BITS, ip_key, salt)
# Prepare UASs and IPs for loading into the models

    # Get a tokenizer trained on the training UAS's
    uas_tokenizer, vocab_size = get_tokenizer(df['uas'])

    # Convert the UAS list into vectors that can be fed to the model
    # Strings are preprocessed for punctionation then enocded with tokenizer
    uas_features = clean_uas(df['uas'], uas_tokenizer)


    # IPs don't need a tokenizer; they are encoded as an array of bits 
    ip_features = clean_ip(df['ip'])
    

    uas_model = get_uas_model(uas_features, uas_labels, vocab_size, UAS_OUTPUT_LAYERS, NUM_UAS_BITS)

    #print(uas_labels)
    #print(ip_labels)
    ip_model = get_ip_model(ip_features, ip_labels, NUM_IP_BITS)

    
    target_key = join_key(uas_key, ip_key)

    #test_df = pd.read_csv(TEST_CSV)

    #def test_models(uas_model, ip_model, uas_features, ip_features, target_labels, target_key):
    #test_models(uas_model, ip_model, uas_features, ip_features, df['is_target'], target_key)

    input_len = len(uas_features[0])
    export_models(uas_model, ip_model, uas_tokenizer, input_len, target_key)
    print("Models written to {}/".format('model'))

if __name__ == "__main__":
    main() 
