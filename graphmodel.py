#!/usr/bin/env python3
from keras.utils import plot_model
from keras.models import load_model

uas_model = load_model('model/uas_model.h5')
ip_model = load_model('model/ip_model.h5')

plot_model(uas_model, to_file='uas_model.png', show_shapes=True, show_layer_names=False)
plot_model(ip_model, to_file='ip_model.png', show_shapes=True, show_layer_names=False)
