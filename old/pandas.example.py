#!/usr/bin/env python3

import pandas as pd
# TODO: use regex (re)
# Take regex from sys.argv (or use argparse)
# Return True if regex matches
# Else return false


def is_target(row):
    ua = row[0]
    # print("UA: {}".format(ua))
    return True

# TODO: Take in CIDR IP range from user
# generate random IP addresses for training data
# is_target returns true if regex matchs and IP in range

# Generate X training examples (user specified in args)

# ? Two columns : one for ua_match, one for ip_match ?
# ? one for both

# ? Read in all from ua.list
# sample randomly X times with replacement from the dataframe to populate a
# training dataframe
# Generate random IP addresses for these instances
# With the training dataframe, apply the is_target function


lines = None
with open("ua.list", 'r') as f:
    lines = f.readlines()
lines = [[x.strip()] for x in lines]
# df = pd.read_csv('test.csv')
df = pd.DataFrame.from_records(lines)

print(df.head())

df['is_target'] = df.apply(is_target, axis=1)

print(df.head())
