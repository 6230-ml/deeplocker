#!/usr/bin/env python3
from sklearn.metrics import classification_report

def get_metrics(y_uas, y_ip, is_target, target_key):
    predictions = [a + b for (a, b) in zip(y_uas, y_ip)]
    y_pred = [p == target_key for p in predictions]

    return classification_report(is_target, y_pred)
