#!/usr/bin/env python3
import pandas as pd
import numpy as np
from numpy import array, asarray, zeros, around, concatenate
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Flatten, Embedding, Dropout
from keras.models import Sequential
from bits import ip_to_bits
from metrics import get_metrics
import re
import random
import pickle
import os

import sys

import hashlib
random.seed()

# Hide those silly TF warnings
from tensorflow.logging import set_verbosity, ERROR
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
set_verbosity(ERROR)
VERBOSITY=1

EPOCHS=2

# constants
UAS_OUTPUT_LAYERS = 10000
IP_OUTPUT_LAYERS = 2000
NUM_UAS_BITS = 32
NUM_IP_BITS = 8

NUM_SALT_BITS = 32

SAVE_NEW_MODEL = True # whether to save the model after training or not

# Get user agent strings from list file
#f = open('ua.list')
#uas = [l for l in f]
#f.close()

# Get IP addresses from list file
#f = open('ip.list')
#ip_addr = array([l for l in f])
#f.close()

# 0/1 labels which indicate whether this example is a valid one
#one_bit_uas_labels = array([0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
#one_bit_ip_labels = array(([0] * 9) + ([1] * 10) + ([0] * 235))


def generate_keys(inputs, labels, num_bits, target_key, salt):
    '''generates random encryption keys for the invalid examples, and all 1s for valid examples'''
    new_labels = []
    for _input, label in zip(inputs, labels):
        if label:
            new_labels.append(target_key)
        else:
            #new_labels.append([random.randint(0, 1) for _ in range(bits)])
            # hashlib.pbkdf2_hmac(hash_name, password, salt, iterations, dklen=None)
            fail_key = hashlib.pbkdf2_hmac('sha512', _input.encode(), salt, 1000, dklen=(num_bits / 8) )
            fail_key = np.unpackbits(bytearray(fail_key))
            #print("Fail Key: {}".format(fail_key))
            new_labels.append(fail_key)
    return array(new_labels)

def rand_key(num_bits):
    return [random.randint(0, 1) for _ in range(num_bits)]

df = pd.read_csv('data.csv')
salt = bytes(rand_key(NUM_SALT_BITS))
uas_key = rand_key(NUM_UAS_BITS)
ip_key = rand_key(NUM_IP_BITS)

uas_labels = generate_keys(df['uas'], df['uas_is_target'], NUM_UAS_BITS, uas_key, salt)

#df['uas_label'] = uas_labels

def to_bit_array(b):
    return np.unpackbits(bytearray(b))

#df['uas_label'] = df['uas_label'].apply(to_bit_array)
#print(df['uas_label'].apply(to_bit_array))

#uas_labels = [to_bit_array(label) for label in uas_labels if df['']]
#for i, label in enumerate(uas_labels):
#    print("Label {}: {}".format(i, label))

print(df.head())
print(df.columns)



# generate the uas encryption keys
#uas_labels = generate_keys(one_bit_uas_labels, NUM_UAS_BITS)

def clean_uas(uas):
    # replace some special characters with spaces
    uas = re.sub(r'[/:.\-]', ' ', uas)

    # strip some special characters
    uas = re.sub(r'[();,[\]]', '', uas)
    return uas

df['uas'] = df['uas'].apply(clean_uas)

# prepare tokenizer
t = Tokenizer()
t.fit_on_texts(df['uas'])
vocab_size = len(t.word_index) + 1

# integer encode the strings
encoded_uas = t.texts_to_sequences(df['uas'])

# pad strings to the longest
max_length = max([ len(l) for l in encoded_uas ])
print(df.head())
print(df.columns)
#df['padded_uas'] = pad_sequences(encoded_uas, maxlen=max_length, padding='post')
padded_uas = pad_sequences(encoded_uas, maxlen=max_length, padding='post')
print(df.head())
print(df.columns)

# define uas model
uas_model = Sequential()
uas_model.add(Embedding(vocab_size, UAS_OUTPUT_LAYERS, input_length=max_length))
uas_model.add(Flatten())
uas_model.add(Dense(NUM_UAS_BITS, activation='sigmoid'))

# compile the model
uas_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])

# fit the model
uas_model.fit(padded_uas, uas_labels, epochs=EPOCHS, verbose=VERBOSITY)

# evaluate the model
# _, uas_accuracy = uas_model.evaluate(padded_uas, uas_labels, verbose=VERBOSITY)
# print('UAS Accuracy: %f' % (uas_accuracy*100))


# translate ip address strings to 32-bit arrays
ip_bits = array([ip_to_bits(i) for i in df['ip']])

# generate encryption keys
#ip_labels = generate_keys(one_bit_ip_labels, NUM_IP_BITS)

ip_labels = generate_keys(df['ip'], df['ip_is_target'], NUM_IP_BITS, ip_key, salt)

#df['ip_label'] = ip_labels

# define ip model
ip_model = Sequential()
ip_model.add(Dense(1000, activation='relu', input_shape=(32,)))
ip_model.add(Dense(1000, activation='relu'))
ip_model.add(Dense(1000, activation='relu'))
ip_model.add(Dense(NUM_IP_BITS, activation='sigmoid'))

# compile the model
ip_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])

# fit the model
ip_model.fit(ip_bits, ip_labels, epochs=EPOCHS, verbose=VERBOSITY)

# evaluate the model
# _, ip_accuracy = ip_model.evaluate(ip_bits, ip_labels, verbose=VERBOSITY)
# print('IP Accuracy: %f' % (ip_accuracy*100))

# evaluate the whole model (uas and ip)
y_uas = around(uas_model.predict(padded_uas)).astype(int).tolist()
y_ip = around(ip_model.predict(ip_bits)).astype(int).tolist()
print(get_metrics(y_uas, y_ip, df['is_target'].tolist(), uas_key + ip_key))



# Save the model
if SAVE_NEW_MODEL:
    os.makedirs('model', exist_ok=True)
    uas_model.save('model/uas-model.h5')
    ip_model.save('model/ip-model.h5')
    with open('model/tokenizer.pickle', 'wb') as handle:
        pickle.dump(t, handle, protocol=pickle.HIGHEST_PROTOCOL)

# How to load the model
# from keras.models import load_model
# uas_model = load_model('model/uas-model.h5')
# ip_model = load_model('model/ip-model.h5')
# with open('tokenizer.pickle', 'rb') as handle:
#     t = pickle.load(handle)
# And then proceed with vocab_size = ... encoded_uas = ...
# t will already be set up
# When ready to get an output do
# uas_model.predict(x) and ip_model.predict(x)
# to get the encryption key.

