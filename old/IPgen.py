#!/usr/bin/env python3

# Use this script to generate a list of host IPs for the specified network ID
# and preferred CIDR. Make sure your network ID is the beginning IP address for
# the desired range. This will calculate your CIDR and error out if the entered
# IP is not the beginning address for that range.

# Once the script completes, it will write to the file 'IP.list'. Rename this
# file to whatever you want to be used for import elsewhere.

# Currently, the files IP-large.list, IP-medium.list, and IP-small.list are
# based off of the host IP 174.80.12.135. Below is a table of the network IDs
# and CIDRs used to generate the lists.

# IP-large.list -  174.80.0.0/16
# IP-medium.list - 174.80.12.0/24
# IP-small.list -  174.80.12.128/29 (most representative of ISP assignment)

# https://docs.python.org/3/howto/ipaddress.html
import ipaddress

# Take user inputs for network ID and CIDR
print('Enter the desired network ID. Make sure NOT to set any host bits.')
print('(Ie. If using a /24 CIDR, zero out the fourth octet, ex. 192.168.1.0)')
netInput = input()

print('What CIDR value do you want to use (ex. 24)?')
print('/', end='')
cidrInput = input()

# Combine the network ID and CIDR notation into one string
netID = netInput + '/' + cidrInput

# Define the network
net = ipaddress.ip_network(netID)

# Confirm the network looks correct
print('You specified the following network:     ', netID)

print('Writing the host addresses for the specified range.')
f = open('IP.list', 'w')
for x in net.hosts():
    f.write(str(x) + '\n')
