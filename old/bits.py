def ip_to_bits(ip):
    ip_n = ip.split('.')
    ip_bits = []
    for i in ip_n:
        j = int(i) # numerical value of i
        # from 7 to 0, decrementing
        for n in range(7, -1, -1):
            k = j // (2**n) # divide by current power of two
            ip_bits.append(k % 2) # current bit is k mod 2, so 0 or 1
            j = j - (k * 2**n) # subtract portion that was examined, if not zero
    return ip_bits
