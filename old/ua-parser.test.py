#!/usr/bin/env python3
# Parser credit goes to https://github.com/ua-parser/uap-python
# https://www.uaparser.org/

from ua_parser import user_agent_parser
import pprint
import pandas
pp = pprint.PrettyPrinter(indent=4)

ua_string = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.104 Safari/537.36'
parsed_string = user_agent_parser.ParseUserAgent(ua_string)

print('*** Printing the parsed UA string. ***')
print('')
pp.pprint(parsed_string)


test = None
with open('ua-short.list', 'r') as f:
    test = f.readlines()

print(test)

# test = [[x.strip()] for x in test]
#
# df = pandas.DataFrame.from_records(test)
#
# print('')
# print('*** Printing the dataframe. ***')
# print('')
# print(df)
