#!/usr/bin/env python3

# This is a proof of concept script to show the abilities of the ipaddress
# module. It begins by taking input from the user, a network ID (first address
# of a network range) and a CIDR notation. Then the script combines these into
# a usable string, confirms the output to the user, displays the number of
# assignable addresses, tests if a supplied IP is within the range, supplies a
# random IP address within the range, and then supplies a random IP address
# that is not within the range by randomly selecting a 32-bit integer and
# running it through the IP conversion function.

# https://docs.python.org/3/howto/ipaddress.html
import ipaddress
import random

# Take user inputs for network ID and CIDR
print('Enter the desired network ID. Make sure NOT to set any host bits.')
print('(Ie. If using a /24 CIDR, zero out the fourth octet, ex. 192.168.1.0)')
netInput = input()

print('What CIDR value do you want to use (ex. 24)?')
print('/', end='')
cidrInput = input()

# Combine the network ID and CIDR notation into one string
netID = netInput + '/' + cidrInput

# Define the network
net = ipaddress.ip_network(netID)
netCount = net.num_addresses

# Confirm the network looks correct
print('You specified the following network:     ', netID)
print('Num of hosts in network:                 ', netCount - 2)

# Take address from user to see if in range
print('Enter an IP to check if in network range: ', end='')
testAddr = ipaddress.ip_address(input())
print('Result: ', end='')
if testAddr in ipaddress.ip_network(net):
    print('Success! That is a valid IP within the range.')
else:
    print('Error! That is not a valid IP within the range.')

# Prints a random IP within the network range
print('Random IP address sample from IP range:   ', end='')
print(net[random.randrange(netCount)])

# Prints a random IP NOT found within the network range
# Generate a random, 32-bit integer and assign it to a variable
randInt = random.randint(0, 4294967295)
# Define the IP address
notIP = ipaddress.ip_address(randInt)

# While Forever check if random IP is in the defined network
# If it is (highly unlikely), randomize the integer again
# If not, print the random IP address
while True:
    if notIP in ipaddress.ip_network(net):
        randInt = random.randint(0, 2147483647)
        notIP = ipaddress.ip_address(randInt)
    else:
        print('Random IP address NOT in range:           ', end='')
        print(ipaddress.ip_address(notIP))
        break

# Testing
# maxInt = 4294967295
# lowInt = 0
# maxIP = ipaddress.ip_address(maxInt)
# lowIP = ipaddress.ip_address(lowInt)
# print('Highest IP possible ', ipaddress.ip_address(maxIP))
# print('Lowest IP possible  ', ipaddress.ip_address(lowIP))
