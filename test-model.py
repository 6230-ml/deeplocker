#!/usr/bin/env python3

from deeplocker_utils import *
from numpy import array, asarray, zeros, around, concatenate

from bitarray import bitarray

import pandas as pd

from sklearn.metrics import classification_report, confusion_matrix

import os
# Hide those silly TF warnings
from os import environ
from tensorflow.logging import set_verbosity, ERROR

environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
set_verbosity(ERROR)

def test_models(uas_model, ip_model, uas_features, ip_features, target_labels, target_key, df):
    i=0
    uas_keys = around(predict(uas_model, uas_features)).astype(int).tolist()
    ip_keys = around(predict(ip_model, ip_features)).astype(int).tolist()
    output_keys = [ join_key(uas_key, ip_key) for uas_key, ip_key in zip(uas_keys, ip_keys) ]

    y_pred = [ k == target_key for k in output_keys ]

    #for key, is_target in zip(output_keys, target_labels):
    #    print("Is Target: {}\tTarget Key: {}\tGenerated Key: {}".format(is_target, target_key, key))

    errors = []
    for i, pred in enumerate(y_pred):
        if pred != target_labels[i]:
            errors.append(i)

    if errors:
        print("ERRORS ({}):".format(len(errors)))
        pd.set_option('display.max_colwidth', -1)
        for i, row in df.iloc[errors].iterrows():
            print(row)

        
    print(classification_report(target_labels, y_pred, digits=6))
    matrix = confusion_matrix(target_labels, y_pred)
    print(matrix)
    tn, fp, fn, tp = matrix.ravel()
    print("TN: {}\nFP: {}\nFN: {}\nTP: {}".format(tn, fp, fn, tp))

#def load_models():
#    uas_model = load_model('model/uas_model.h5')
#    ip_model = load_model('model/ip_model.h5')
#    tokenizer = None
#    with open('model/tokenizer.pickle', 'rb') as handle:
#        tokenizer = pickle.load(handle)
#    target_key = None
#    with open('model/target.key', 'rb') as handle:
#        target_key = handle.read()
#    input_len = None
#    with open('model/input.len', 'r') as handle:
#        input_len = int(handle.read())
#
#    return uas_model, ip_model, tokenizer, input_len, target_key

def get_csv_list():
    files = []
    for dirpath, dirnames, filenames in os.walk('test-data'):
        files.extend( [os.path.join(dirpath, f) for f in filenames if not f.startswith('.') ] )
        break
    return files
        

def main():
    uas_model, ip_model, tokenizer, input_len, target_key = load_models()
    for csv in get_csv_list():
        print("Testing against: {}".format(csv))
        df = pd.read_csv(csv)
        
        # Convert the UAS list into vectors that can be fed to the model
        # Strings are preprocessed for punctionation then enocded with tokenizer
        uas_features = clean_uas(df['uas'], tokenizer, max_length=input_len)

        # IPs don't need a tokenizer; they are encoded as an array of bits 
        ip_features = clean_ip(df['ip'])

        test_models(uas_model, ip_model, uas_features, ip_features, df['is_target'], target_key, df)

if __name__=="__main__":
    main()
